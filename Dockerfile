FROM node:13

LABEL maintainer="Stratos Gerakakis <stratosgear@gmail.com>"

# Install semantic-release base and plugins
RUN npm install @semantic-release/gitlab \
    @semantic-release/exec \
    @semantic-release/changelog \
    @semantic-release/git \
    @google/semantic-release-replace-plugin

RUN npx semantic-release --version    